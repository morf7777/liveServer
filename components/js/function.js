const searchInput = $(".search")
const users= $(".users")
const objUsers=[]
// functions for making user elements and adding to dom
const userMaker = mixer(addToDOM,userLocation,userName,userimg,elementMaker)
function mixer(...fns){return fns.reduce(compose)}
function compose(func2,func1){
    return function(...args){return func2(func1(...args))}
}
function elementMaker(obj){
    let element = $('<div>').addClass("users__user");
    return [element,obj];
}
function userimg([element,obj]){
    let img = $('<img>').attr("src",obj.picture.large).addClass("users__img")
    return [element.append(img),obj]
}
function userName([element,obj]){
    let uname=$('<h2>').text(obj.name.first+" "+obj.name.last);
    return [element.append(uname),obj]
}
function userLocation([element,obj]){
    let ulocation = $('<p>').text(obj.location.country+"-"+obj.location.state+"-"+obj.location.city+"-"+obj.location.street.name+"-"+obj.location.street.number)
    return [element.append(ulocation),obj]
}
function addToDOM (element,obj){
    users.append(element);
}
// first search functions (slow)
function setingOrder(array,string){
    $.each(array,function(index,item){
        let firstname=item.name.first.toLowerCase().search(string.toLowerCase());
        let lastname=item.name.last.toLowerCase().search(string.toLowerCase());
        let origin=item.location.city.toLowerCase().search(string.toLowerCase());
        if(lastname <= Math.abs(firstname) && lastname <= Math.abs(origin) && lastname>-1){
            item.order=lastname;
        }else if(firstname <= Math.abs(origin) && firstname >-1){
            item.order=firstname+0.5;
        }else if (origin>-1){
            item.order=origin+0.7;
        }else{
            item.order=-1;
        }
    })
    return array;
}
// function sorting(array){
//     let firstArray=[];
//     let secondArray=[];
//     let thirdArray=[];
//     let forthArray=[];
//     for(let i=0 ; i<array.length ; i++){
//         let value=array[i].order
//         if(value===0){
//             firstArray.push(array[i])
//         }else if(value<1 &&value>0){
//             secondArray.push(array[i])
//         }else if(value<2 &&value[i]>0){
//             thirdArray.push(array[i])
//         }else{
//             forthArray.push(array[i])
//         }
//     }
//     return firstArray.concat(secondArray,thirdArray,forthArray)
// }
// second search function (fast)
function sorting(array){
    let firstArray=[];
    let secondArray=[];
    let thirdArray=[];
    let forthArray=[];
    for(let i=0 ; i<array.length ; i++){
        let value=array[i].order
        if(value===0){
            firstArray.push(array[i])
        }else if(value<1 &&value>0){
            secondArray.push(array[i])
        }else if(value<2 &&value[i]>0){
            thirdArray.push(array[i])
        }
    }
    return firstArray.concat(secondArray,thirdArray)
}
// functions for object

// object maker for data
// $.ajax({
//     url: 'https://randomuser.me/api/?page=3&results=10&seed=abc',
//     dataType: 'json',
//     success: function(data) {
//       console.log(data);
//     }
//   });

$(function(){
    let myurl = 'https://randomuser.me/api/?page=3&results=1000&seed=abc&inc=name,picture,location';
    let mydata=[];
    $.getJSON(myurl).done(function(data){
        $.each(data.results,function(index,item){
            mydata.push(item);
        })
        $.each(mydata,function(index,item){
            userMaker(item)
        })
    })
    searchInput.keyup(function(event){
        let myvalue=searchInput.val()
        let mySecondData=setingOrder(mydata,myvalue)
        mySecondData=sorting(mydata)
        users.empty()
        $.each(mySecondData,function(index,item){
            userMaker(item)
        })
    })
})

